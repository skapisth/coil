function timings=timeProcess(tagData,params,eye)

    
    trialTime = trialTiming(tagData,0);
    trialTime = [trialTime [1:size(trialTime,1)]']; 
    timings.trialTime = trialTime;
    trialTime
    
    pattern = 'trials_';
    fields = fieldnames(params);
    params2Cell = struct2cell(params);
    check_reqdStrings = startsWith(fields,pattern);
    reqdIdxs = find(check_reqdStrings == 1);
    
    for numTasks = 1:length(reqdIdxs)
        taskName = fields{reqdIdxs(numTasks)};
        split_taskName = split(taskName, 'trials_');
        varName_init = string(split_taskName{2}) + '_';
        
        num_taskTrials = params2Cell{reqdIdxs(numTasks)};
        
        for numTaskTrials = 1:num_taskTrials
            varName1 = sprintf(varName_init + string(numTaskTrials));
            eval([varName1 + '=' + input("which row in trial time corresponds to " +  string(numTaskTrials) + " " + string(split_taskName{2}) +'??')']);
            varName2 = "timings" + "." + split_taskName{2} + '{' + string(numTaskTrials) + '}' + '.trial';
            varName3 = "trialTime" + '(' + string(varName1) + ',' + ':' + ')';
            eval(varName2{1} + "=" + string(varName3))
            
        end
    end
    
sort_rowId_1 = input("which row to use for first sorting time?");
sort_rowId_2 = input("which row to use for second sorting time?");

timings.sorting{1}.trial=trialTime(sort_rowId_1,:); %1
timings.sorting{2}.trial=trialTime(sort_rowId_2,:); %2

eyeProbe_rowId = input("which row to use for eye probe?");
timings.eyeProbe.trial=trialTime(eyeProbe_rowId,:);
%eyeIdx = 1;


%% extract fixation from a trial
trialIdx = input("which idx of the trial do you want to look into?"); %2 
%thre = 5E-3;
trialDur = timings.head_Free_9pt_Calib{trialIdx}.trial(1):timings.head_Free_9pt_Calib{trialIdx}.trial(2);
trans=[0 find(diff(eye.coil_vel{params.eyeIdx}(trialDur)>params.threshold)) length(trialDur)]; 
transIdx=find(diff(trans)>500);

figure; %%% WON'T WORK AUTOMATICALLY (MAY BE ASK USER BEGINNING AND END OF FIXATION POINT)
subplot(2,1,1); plot(eye.coil_sync{1}(:,trialDur)');
subplot(2,1,2); hold on;
plot(eye.coil_vel{params.eyeIdx}(trialDur))
for i=length(transIdx):-1:1
    line([trans(transIdx(i)) trans(transIdx(i))],[0 max(eye.coil_vel{params.eyeIdx}(trialDur))],'color','r');
    line([trans(transIdx(i)+1) trans(transIdx(i)+1)],[0 max(eye.coil_vel{params.eyeIdx}(trialDur))],'color','b');
end
if length(transIdx)~= params.numCalibPts
    transIdx(1)=[]; 
end

for ptIdx = params.numCalibPts:-1:1
    timings.head_Free_9pt_Calib{trialIdx}.fix(ptIdx,1)=trans(transIdx(ptIdx))+trialDur(1);
    timings.head_Free_9pt_Calib{trialIdx}.fix(ptIdx,2)=trans(transIdx(ptIdx)+1)+trialDur(1);
end

for ptIdx = params.numCalibPts:-1:2
    timings.head_Free_9pt_Calib{trialIdx}.fix(ptIdx-1,1)=trans(transIdx(ptIdx))+trialDur(1);
    timings.head_Free_9pt_Calib{trialIdx}.fix(ptIdx-1,2)=trans(transIdx(ptIdx)+1)+trialDur(1);
end
timings.head_Free_9pt_Calib{trialIdx}.fix(9,1)=trans(transIdx(9)+1)+trialDur(1);
timings.head_Free_9pt_Calib{trialIdx}.fix(9,2)=trialDur(end);

%%
disp('saving data');
save([params.inputPath 'processed_timings.mat'],'timings');
disp('Done!')
