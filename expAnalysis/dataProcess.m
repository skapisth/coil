addpath('..\tools\')

direct = 'C:/Users/sanjana/OneDrive - University of Rochester/Documents/Sanjana/Projects/coil/fieldCalibrate/data/';
date = '06-Mar-2020';
session = 'run3';

inputPath = [direct date '/' session '/'];
outputPath = [inputPath '/Figures/']; 
if ~exist(outputPath)
    mkdir(outputPath); 
end

VIEW =1;
SAVE_Fig =1;

%% load
load('params.mat')


params.VIEW = VIEW; params.SAVE = SAVE_Fig; 
params.date = date; 
params.inputPath = direct; params.outputPath = outputPath;
params.session = session;

%% loading data


[objects,optiData,tagData,coilData,optSyncIdx] = loadData(params);


%% print out and define the names of all objects
for i=1:length(objects)
   {i, objects{i}.name};
   
   if strcmp(objects{i}.name, 'ninePts')
       ninePoints = objects{i};
   elseif strcmp(objects{i}.name, 'table')
       table = objects{i};
   elseif strcmp(objects{i}.name,'tableGrid')
       tableGrid = objects{i};
   elseif strcmp(objects{i}.name,'helmet')
       helmet = objects{i};
   elseif strcmp(objects{i}.name, 'eyeProbe')
       eyeProbe = objects{i};
   elseif strcmp(objects{i}.name, 'iceTray')
       iceTray = objects{i};
   elseif strcmp(objects{i}.name, 'headRest')
       headRest = objects{i};
   elseif strcmp(objects{i}.name, 'head_eye')
       head_eye = objects{i};
   end
end

%% head and eye coil 
for coilIdx=params.eyeChannel
    if ~isempty(coilData.sig_syncR{coilIdx})... 
    && sum(coilData.sig_syncR{coilIdx}(1,:)<0)/length(coilData.sig_syncR{coilIdx}(1,:))<0.4 ...
    && sum(coilData.sig_syncR{coilIdx}(2,:)<0)/length(coilData.sig_syncR{coilIdx}(2,:))<0.4
        eye.coil_sync{coilIdx}=-coilData.sig_syncR{coilIdx};
        eye.coil_1k{coilIdx}=-coilData.sig_1kR{coilIdx}; 
    else
        eye.coil_sync{coilIdx}=coilData.sig_syncR{coilIdx};
        eye.coil_1k{coilIdx}=coilData.sig_1kR{coilIdx}; 
    end
end

for coilIdx=params.headChannel
    head.coil_sync{coilIdx}=coilData.sig_syncR{coilIdx}; 
    head.coil_1k{coilIdx}=coilData.sig_1kR{coilIdx}; 
end


%% timing 
data2plot{1}=helmet.pos-mean(helmet.pos,2);  data2plot{2}=helmet.q; data2plot{3}=eye.coil_sync{2};
taggingPlot(tagData.t_sync,data2plot, {'helmet position', 'helmet rotation', 'eye coil'}, tagData);

disp('Entering time process');
timings = timeProcess(tagData,params,eye);

disp('Entering object proty6fgcess');
objectProcess

disp('Entering head eye process');
headEyeProcess

% ninePtGrid_eyeCalib
% 
% tableGrid_eyeCalib
