Analysis code to process coil data
NOTE - 
1) Previous timProcess.m is now timeProcess (script completely changed)
2) tagging_plot.m changed to taggingPlot (to maintain name consistency)
3) trial_timing.m changed to trialTiming (to maintain name consistency)
4) head_process.m changed to headProcess (to maintain name consistency)
5) eye_position changed to eyePosition 

TODO -

1) Go to objectProcess
2) Change path to data folder in params to your path.
3) Run this to see how the old code has been automated.
4) Run timeProcess to get timing detailed matfiles.
5) Run headEyeProcess to get head and eye detailed mat files.
